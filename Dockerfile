FROM hashicorp/packer:light
RUN mkdir  /etc/ansible \
          && apk -U --no-cache add ansible
COPY ansible.cfg /etc/ansible/ansible.cfg
